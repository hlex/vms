# September 9th, 2018, Mondit Thumniramon (18.09.09)
- Add data log for mobile topup flow.
- Remove id and discount from submit order data.

# August 28th, 2018 (18.08.28)
- แก้ไขเงื่อนไขการส่งคำสั่ง hard reset ไปที่ hardware box
- ส่ง advertisement log ไปยัง cloud server

# August 18th, 2018, Mondit Thumniramon (18.08.19)

- เพิ่ม Interval ระหว่างการสแกน barcode หรือ QR code, line id จะทำงานห่างกัน 1.5 วินาที (ยิงรัวๆ มาภายใน 1.5 วิก็จะนับเป็น 1 ครั้งเท่านั้น)
- แก้ไขปัญหาไม่สามารถ submit order กิจกรรม ถั่ว ลดราคา 3 บาทได้
- แก้ไขปัญหาการส่งข้อมูลไป save log กับ Cloud API ส่งข้อมูลผิด Format เล็กน้อย
- handle การ scan จากเครื่องอ่าน จะสามารถใช้งานรหัสที่อ่านมาได้ เมื่อโปรแกรมอยู่ในหน้ากิจกรรมที่อนุญาต (หน้าที่ให้คีย์ข้อมูลการเล่นกิจกรรม) และ หน้า Verify Salesman เท่านั้น กรณีอื่นๆ ที่อ่านมา โปรแกรมจะไม่ตอบสนอง
