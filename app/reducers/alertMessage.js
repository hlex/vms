import _ from 'lodash';

import {
  OPEN_ALERT_MESSAGE,
  CLOSE_ALERT_MESSAGE,
} from '../actions/actionTypes';

const initialState = {
  show: false,
  messages: {
    title: '',
    th: '',
    en: ''
  },
  // messages: {
  //   title: 'ขอบคุณที่ร่วมกิจกรรม',
  //   th: `รหัสส่วนลด คือ xxxxxxx มูลค่า 10 บาท (ใช้ได้ก่อนวันที่ 10-09-2018)`,
  //   en: `Discount code is xxxxxxx value 10 Baht (ใช้ได้ก่อนวันที่ 10-09-2018)`
  // },
};
const getInitialState = () => ({
  ...initialState,
});

export default (state = getInitialState(), action) => {
  switch (action.type) {
    case OPEN_ALERT_MESSAGE:
      return {
        ...state,
        show: true,
        messages: action.data.messages,
      };
    case CLOSE_ALERT_MESSAGE:
      return getInitialState();
    default:
      return state;
  }
};
