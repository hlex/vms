import _ from 'lodash';
import {
  RESET_APPLICATION,
  RECEIVED_CASH,
  RECEIVED_CASH_COMPLETELY,
  PRODUCT_DROP_PROCESS_COMPLETELY,
  RESET_PAYMENT_REDUCER,
  CLEAR_PAYMENT_AMOUNT,
  RECEIVED_CASH_REMAINING,
  SET_CASH_CHANGE_AMOUNT,
  RECEIVED_PAID_IN_FULL,
  CLEAR_RECEIVED_PAID_IN_FULL
} from '../actions/actionTypes';

const initialState = {
  isLoading: false,
  isFinish: false,
  amount: 0,
  cashboxAmount: 0,
  tubeAmount: 0,
  remain: {},
  cashChangeAmount: 0,
  isReceivedPaidInFull: false,
  canGoBack: true
};
const getInitialState = () => {
  return {
    ...initialState
  };
};


export default function products(state = getInitialState(), action: actionType) {
  switch (action.type) {
    case RESET_APPLICATION:
    case RESET_PAYMENT_REDUCER:
      return {
        ...getInitialState(),
        amount: state.amount,
        remain: state.remain,
        cashChangeAmount: state.cashChangeAmount,
      };
    case RECEIVED_CASH:
      const insertedType = action.data.dest || 'cashbox';
      const insertedValue = Number(action.data.msg);
      const addValue = _.isNaN(insertedValue) ? 0 : insertedValue;
      return {
        ...state,
        amount: state.amount + addValue,
        cashboxAmount: insertedType === 'cashbox' ? state.cashboxAmount + addValue : state.cashboxAmount,
        tubeAmount: insertedType === 'tube' ? state.tubeAmount + addValue : state.tubeAmount,
        canGoBack: false
      };
    case 'CANNOT_GO_BACK':
      return {
        ...state,
        canGoBack: false
      };
    case 'CAN_GO_BACK':
      return {
        ...state,
        canGoBack: true
      };
    case RECEIVED_CASH_COMPLETELY:
      return {
        ...state,
        isLoading: true,
      };
    case PRODUCT_DROP_PROCESS_COMPLETELY:
      return {
        ...state,
        isFinish: true,
      };
    case CLEAR_PAYMENT_AMOUNT:
      return {
        ...state,
        amount: 0,
        cashboxAmount: 0,
        tubeAmount: 0,
      };
    case RECEIVED_CASH_REMAINING:
      return {
        ...state,
        remain: action.data.remain
      };
    case SET_CASH_CHANGE_AMOUNT:
      return {
        ...state,
        cashChangeAmount: action.cashChangeAmount
      };
    case RECEIVED_PAID_IN_FULL:
      return {
        ...state,
        isReceivedPaidInFull: true
      };
    case CLEAR_RECEIVED_PAID_IN_FULL:
      return {
        ...state,
        isReceivedPaidInFull: false
      };
    default:
      return state;
  }
}
