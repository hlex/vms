import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
// ======================================================
// Actions
// ======================================================
import * as ApplicationActions from '../../../actions/applicationActions';
// ======================================================
// Selectors
// ======================================================
import * as OrderSelector from '../../../selectors/order';

const mapStateToProps = state => {
  return {
    isEventOrder: OrderSelector.verifyIsEventOrder(state.order),
    canGoBack: state.payment.canGoBack
  };
};

const actions = {
  ...ApplicationActions,
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

class App extends Component {
  static propTypes = {
    back: PropTypes.string.isRequired,
    canGoBack: PropTypes.bool.isRequired,
  }

  handleClickBack = () => {
    const { back, canGoBack } = this.props;
    if (canGoBack === true) {
      back();
    } else {
      console.warn('Cannot go back');
    }
  }

  render() {
    return (
      <div className="action-back">
        <a
          className="button purple M"
          onClick={this.handleClickBack}
        >
          <i className="fa fa-chevron-left" />ย้อนกลับ
        </a>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
