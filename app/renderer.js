const ipc = require('electron').ipcRenderer;

window.closeApp = function (args) {
  ipc.send('close-app', args);
  window.close();
};

window.restartApp = function (args) {
  ipc.send('restart-app', args);
  // window.close();
};
