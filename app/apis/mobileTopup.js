import { handleResponseCatchError, convertToURLParam } from 'api-jarvis';
import { fetchFacade } from '../helpers/api';
import { isVMSServiceError, convertVMSServiceResponseToError } from '../helpers/error';
import URL from './url';

const FORM_CODE = '8HPyBox@1Vm';

export const serviceTopupMobile = (
  {
    serviceCode,
    MSISDN,
    mobileTopupValue,
    mobileTopupFee
  },
  discountCode = '',
  discountPrice = 0,
  machineId = '',
  trxId = ''
) => {
  const data = {
    ServiceCode: serviceCode,
    MobileNo: MSISDN,
    TopupAmount: mobileTopupValue,
    ServiceCharge: mobileTopupFee,
    Discount_Code: discountCode,
    Discount_Price: discountPrice,
    MachineID: machineId,
    RequestFrom: FORM_CODE,
    LogID: trxId,
  };

  // return new Promise((resolve, reject) => {
  //   setTimeout(() => {
  //     // resolve({
  //     //   'response-data': {},
  //     //   status: "SUCCESSFUL",
  //     //   'trx-id': "20180623121628",
  //     //   'fault': {}
  //     // });
  //     reject({
  //       status: 'SUCCESSFUL',
  //       'trx-id': '',
  //       fault: {
  //         vtype: 'check data',
  //         'th-message': 'ข้อมูลไม่ถูกต้อง',
  //         'en-message': 'error'
  //       }
  //     });
  //   }, 1000 * 10);
  // });
  return fetchFacade(`${URL.mobileTopup}${convertToURLParam(data)}`).then((response) => {
    handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
    return response;
  });
};
