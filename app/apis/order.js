import _ from 'lodash';
import { handleResponseCatchError, convertToURLParam } from 'api-jarvis';
import { fetchFacade } from '../helpers/api';
import { isVMSServiceError, convertVMSServiceResponseToError } from '../helpers/error';
import { appLog, getArrData } from '../helpers/global';
import URL from './url';

export const serviceSubmitOrder = ({
  id,
  poId,
  saleType,
  discountCode,
  discountPrice,
  qty,
  unitPrice,
  slotNo,
  barcode,
  lineQrcode
}) => {
  // return new Promise((resolve, reject) => {
  //   setTimeout(() => {
  //     resolve(true);
  //   }, 1000);
  // });
  const data = {
    vtype: 'order',
    id,
    po_id: poId,
    saletype: saleType,
    discountcode: discountCode,
    discountprice: discountPrice,
    qty,
    unitprice: unitPrice,
    slotno: slotNo,
    barcode,
    lineqrcode: lineQrcode
  };
  return fetchFacade(`${URL.submitOrder}${convertToURLParam(data)}`, { local: true }).then(
    response => {
      handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
      return response;
    }
  );
};

export const serviceGetSumOrderAmount = () => {
  const data = {
    vtype: 'sumOrderAmount'
  };
  return fetchFacade(`${URL.sumOrderAmount}${convertToURLParam(data)}`, { local: true }).then(
    response => {
      handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
      return response;
    }
  );
};

export const syncSettlement = ({ salesman, remainingCoinsString }) => {
  const data = {
    vtype: 'SettleMent',
    SaleMan: salesman,
    remainingCoins: remainingCoinsString // '1|20,5|30,10|50'
  };
  return fetchFacade(`${URL.syncSettlement}${convertToURLParam(data)}`, { local: true }).then(
    response => {
      handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
      return response;
    }
  );
};

export const updateStock = () => {
  const data = {
    vtype: 'UpdateStock'
  };
  return fetchFacade(`${URL.updateStock}${convertToURLParam(data)}`, { local: true }).then(
    response => {
      handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
      return response;
    }
  );
};

export const getRequestOrderId = () => {
  const data = {
    vtype: 'getRequestOrderId'
  };
  return fetchFacade(`${URL.updateStock}${convertToURLParam(data)}`, { local: true }).then(
    response => {
      handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
      return response;
    }
  );
};

export const saveTransactionOrder = ({ machineId, trxId, data }) => {
  const urlParams = {
    vtype: 'LogTransaction',
    machine_id: machineId,
    LogID: trxId,
    arrData: getArrData(data)
  };
  appLog(`saveTransactionOrder = ${trxId} | api/main.php${convertToURLParam(urlParams)}`, '', data, '#39CCCC', '#000000');
  return fetchFacade(`api/main.php${convertToURLParam(urlParams)}`).then(response => {
    handleResponseCatchError(response, isVMSServiceError, convertVMSServiceResponseToError);
    return response;
  });
};
